use 5.014;
use ExtUtils::MakeMaker;

# See lib/ExtUtils/MakeMaker.pm for details of how to influence
# the contents of the Makefile that is written.
WriteMakefile(
    NAME         => 'Lemonldap::NG::Portal',
    VERSION_FROM => 'lib/Lemonldap/NG/Portal.pm',
    LICENSE      => 'gpl',
    META_MERGE   => {
        'recommends' => {
            'DBI'                   => 0,
            'Email::Sender'         => 1.300027,
            'GD::SecurityImage'     => 0,
            'Glib'                  => 0,
            'HTTP::Message'         => 0,
            'Image::Magick'         => 0,
            'Lasso'                 => '2.3.0',
            'LWP::UserAgent'        => 0,
            'LWP::Protocol::https'  => 0,
            'MIME::Entity'          => 0,
            'Net::Facebook::Oauth2' => 0,
            'Net::LDAP'             => 0.38,
            'Net::OAuth'            => 0,
            'Net::OpenID::Consumer' => 0,
            'Net::OpenID::Server'   => 0,
            'SOAP::Lite'            => 0,
            'String::Random'        => 0,
            'Unicode::String'       => 0,
            'Web::ID'               => 0,
        },
    },
    BUILD_REQUIRES => {
        'Email::Sender'         => 0,
        'HTTP::Message'         => 0,
        'IO::String'            => 0,
        'LWP::UserAgent'        => 0,
        'MIME::Entity'          => 0,
        'SOAP::Lite'            => 0,
        'String::Random'        => 0,
    },
    PREREQ_PM => {
        'Clone'                  => 0,
        'Lemonldap::NG::Handler' => '2.0.0',
        'Regexp::Assemble'       => 0,
    },
    (
        $] >= 5.005
        ?    ## Add these new keywords supported since 5.005
          (
            ABSTRACT_FROM =>
              'lib/Lemonldap/NG/Portal.pm',    # retrieve abstract from module
            AUTHOR =>
'Xavier Guimard <x.guimard@free.fr>, Clément Oudot <clement@oodo.net>'
          )
        : ()
    ),
);
