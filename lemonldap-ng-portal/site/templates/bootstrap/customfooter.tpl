  <!-- Custom HTML footer -->
    <p>
      <span trspan="serviceProvidedBy">Service provided by</span>
      <a href="http://lemonldap-ng.org" target="_blank" class="btn btn-default btn-xs">
        <span class="glyphicon glyphicon-new-window"></span>
        LemonLDAP::NG
      </a>
      <span trspan="gplSoft">free software covered by the GPL license</span>.
    </p>
